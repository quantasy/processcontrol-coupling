package ch.quantasy.hyperdrive.blinking.mqtt;

/**
 *
 * @author reto
 */
public record Will(String topic, byte[] first, byte[] last) {
    
}
